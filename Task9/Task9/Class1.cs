﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task9
{
    class Animal
    {
        private string name;
        private string color;
        private string sound;

        public string Name { get => name; set => name = value; }
        public string Color { get => color; set => color = value; }
        public string Sound { get => sound; set => sound = value; }
        public Animal(string name = "unknown")
        {
            Name = name;
        }

        public Animal(string name, string color, string sound)
        {
            Name = name;
            Color = color;
            Sound = sound;
        }
        public void Shout()
        {
            Console.WriteLine(Sound);
        }

    }
}
