﻿using System;
using System.Collections.Generic;

namespace Task9
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> Listofanimals = new List<Animal>();
            Animal dog = new Animal("Helge", "White", "barf");
            Animal cow = new Animal("Trude", "black and white", "moooo");
            Animal fox = new Animal("foxxy", "red", "whapapapapapap pow");
            Listofanimals.Add(dog);
            Listofanimals.Add(cow);
            Listofanimals.Add(fox);
            Console.WriteLine(Listofanimals.Count);
            foreach (Animal name in Listofanimals)
            {
                name.Shout();
            }
        }
    }
}
